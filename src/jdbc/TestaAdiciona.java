package jdbc;

import dao.DonoDao;
import modelo.Dono;

public class TestaAdiciona {

	public static void main(String[] args) {

		// pronto para gravar
	       Dono dono = new Dono();
	       dono.setNome("Andresa");
	       dono.setEmail("andresa@hot");
	       dono.setRua("R. Vergueiro");
           dono.setSenha("1234");
           dono.setTelefone("47443300");
           dono.setCelular("1212121");
           dono.setBairro("jd imperador");
           dono.setCidade("suzano");
           dono.setEstado("sp");

	       // grave nessa conex�o!!!
	       DonoDao dao = new DonoDao();

	       // m�todo elegante
	       dao.adiciona(dono);

	       System.out.println("Gravado!");

	}

}
