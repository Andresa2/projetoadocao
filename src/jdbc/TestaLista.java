package jdbc;

import java.util.List;

import dao.DonoDao;
import modelo.Dono;

public class TestaLista {

	public static void main(String[] args) {

		DonoDao dao = new DonoDao();
		
		List<Dono> donos = dao.getLista();
		
		for (Dono dono : donos) {
			System.out.println("Nome: " + dono.getNome());
			System.out.println("Email: " + dono.getEmail());
		}
	}
}
