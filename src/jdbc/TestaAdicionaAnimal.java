package jdbc;

import dao.AnimalDao;
import modelo.Animal;

public class TestaAdicionaAnimal {

	public static void main(String[] args) {
		// pronto para gravar
	       Animal animal = new Animal();
	       animal.setApelido("Andréa");
	       animal.setIdade(3);
	       animal.setSexo("fêmea");
	       animal.setIdDono((long) 1);

	       // grave nessa conex�o!!!
	       AnimalDao dao = new AnimalDao();

	       // m�todo elegante
	       dao.adiciona(animal);

	       System.out.println("Gravado!");
	}

}
