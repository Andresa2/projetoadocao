package jdbc;

import java.util.List;

import dao.AnimalDao;
import modelo.Animal;

public class TestaListaAnimal {

	public static void main(String[] args) {

		AnimalDao dao = new AnimalDao();

		List<Animal> animais = dao.getLista();

		for (Animal animal : animais) {
			System.out.println("Nome: " + animal.getApelido());
			System.out.println("Sexo: " + animal.getSexo());
		}
	}
}
