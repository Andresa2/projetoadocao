package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DonoDao;
import modelo.Dono;

/**
 * Servlet implementation class AdicionaDonoServlet
 */
@WebServlet("/AdicionaDono")
public class AdicionaDonoServlet extends HttpServlet {

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// busca o writer
		PrintWriter out = response.getWriter();

		// buscando os par�metros no request
		request.setCharacterEncoding("UTF-8");
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		String senha = request.getParameter("senha");
		String telefone = request.getParameter("telefone");
		String celular = request.getParameter("celular");
		String rua = request.getParameter("rua");
		String bairro = request.getParameter("bairro");
		String cidade = request.getParameter("cidade");
		String estado = request.getParameter("estado");

		// monta um objeto dono
		Dono dono = new Dono();
		dono.setNome(nome);
		dono.setEmail(email);
		dono.setSenha(senha);
		dono.setTelefone(telefone);
		dono.setCelular(celular);
		dono.setRua(rua);
		dono.setBairro(bairro);
		dono.setCidade(cidade);
		dono.setEstado(estado);

		// salva o dono
		DonoDao dao = new DonoDao();
		dao.adiciona(dono);

		// imprime o nome do dono que foi adicionado
		out.println("<html>");
		out.println("<body>");
		out.println("dono " + dono.getIdDono() + " adicionado com sucesso");
		out.println("</body>");
		out.println("</html>");
	}

}
