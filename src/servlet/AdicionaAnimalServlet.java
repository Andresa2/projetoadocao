package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Animal;
import dao.AnimalDao;

/**
 * Servlet implementation class AdicionaAnimalServlet
 */
@WebServlet("/AdicionaAnimal")
public class AdicionaAnimalServlet extends HttpServlet {

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// busca o writer
		PrintWriter out = response.getWriter();

		// buscando os par�metros no request
		request.setCharacterEncoding("UTF-8");
		String apelido = request.getParameter("apelido");
		Long idDono = Long.parseLong(request.getParameter("idDono"));
		String sexo = request.getParameter("sexo");
		String raca = request.getParameter("raca");
		String tipoAnimal = request.getParameter("tipoAnimal");
		String porte = request.getParameter("porte");
		Integer idade = Integer.parseInt(request.getParameter("idade"));
		Integer tempoProcurando = Integer.parseInt(request.getParameter("tempoProcurando"));
		String vermifugado = request.getParameter("vermifugado");
		String castrado = request.getParameter("castrado");
		String vacinado = request.getParameter("vacinado");
		String informacoes = request.getParameter("informacoes");
		String foto = request.getParameter("foto");



		// monta um objeto animal
		Animal animal = new Animal();
		animal.setApelido(apelido);
		animal.setIdDono(idDono);
		animal.setSexo(sexo);
		animal.setRaca(raca);
		animal.setTipoAnimal(tipoAnimal);
		animal.setPorte(porte);
		animal.setIdade(idade);
		animal.setTempoProcurando(tempoProcurando);
		animal.setVermifugado(vermifugado);
		animal.setCastrado(castrado);
		animal.setVacinado(vacinado);
		animal.setInformacoes(informacoes);
		animal.setFoto(foto);


		// salva o animal
		AnimalDao dao = new AnimalDao();
		dao.adiciona(animal);

		// imprime o apelido do animal que foi adicionado
		out.println("<html>");
		out.println("<body>");
		out.println("animal " + animal.getApelido() + " adicionado com sucesso");
		out.println("</body>");
		out.println("</html>");
	}

}
