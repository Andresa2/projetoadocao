package modelo;

import java.util.Collection;

public class Dono {
	
	private Long idDono;
    private String nome;
    private String email;
    private String senha;
    private String telefone;
    private String celular;
    private String rua;
    private String bairro;
    private String cidade;
    private String estado;
    private Collection<Animal> animais;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Collection<Animal> getAnimais() {
		return animais;
	}
	public void setAnimais(Collection<Animal> animais) {
		this.animais = animais;
	}
	public Long getIdDono() {
		return idDono;
	}
	public void setIdDono(Long idDono) {
		this.idDono = idDono;
	}
}
