package modelo;

public class Animal {

		private Long idanimal;
		private Long idDono;
		private String apelido;
		private String sexo;
		private String raca;
		private String tipoAnimal;
		private String porte;
		private Integer idade;
		private int tempoProcurando;
		private String vermifugado;
		private String castrado; 
		private String vacinado;
		private String informacoes;
		private String foto;
		
		public Long getIdanimal() {
			return idanimal;
		}
		public void setIdanimal(Long idanimal) {
			this.idanimal = idanimal;
		}
		public String getApelido() {
			return apelido;
		}
		public void setApelido(String apelido) {
			this.apelido = apelido;
		}
		public String getSexo() {
			return sexo;
		}
		public void setSexo(String sexo) {
			this.sexo = sexo;
		}
		public String getRaca() {
			return raca;
		}
		public void setRaca(String raca) {
			this.raca = raca;
		}
		public String getTipoAnimal() {
			return tipoAnimal;
		}
		public void setTipoAnimal(String tipoAnimal) {
			this.tipoAnimal = tipoAnimal;
		}
		public String getPorte() {
			return porte;
		}
		public void setPorte(String porte) {
			this.porte = porte;
		}
		public Integer getIdade() {
			return idade;
		}
		public void setIdade(Integer idade) {
			this.idade = idade;
		}
		public int getTempoProcurando() {
			return tempoProcurando;
		}
		public void setTempoProcurando(int tempoProcurando) {
			this.tempoProcurando = tempoProcurando;
		}
		public String getVermifugado() {
			return vermifugado;
		}
		public void setVermifugado(String vermifugado) {
			this.vermifugado = vermifugado;
		}
		public String getCastrado() {
			return castrado;
		}
		public void setCastrado(String castrado) {
			this.castrado = castrado;
		}
		public String getVacinado() {
			return vacinado;
		}
		public void setVacinado(String vacinado) {
			this.vacinado = vacinado;
		}
		public String getInformacoes() {
			return informacoes;
		}
		public void setInformacoes(String informacoes) {
			this.informacoes = informacoes;
		}
		public String getFoto() {
			return foto;
		}
		public void setFoto(String foto) {
			this.foto = foto;
		}
		public Long getIdDono() {
			return idDono;
		}
		public void setIdDono(Long idDono) {
			this.idDono = idDono;
		}
}

