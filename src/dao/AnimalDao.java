package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.ConnectionFactory;
import modelo.Animal;

public class AnimalDao {
	
	
	// a conex�o com o banco de dados
    private Connection connection;

    public AnimalDao() {
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public void adiciona(Animal animal) {
        String sql = "insert into animais " +
                "(apelido,fkdono,sexo,raca,tipoAnimal,porte,idade,tempoProcurando,vermifugado,castrado,vacinado,informacoes,foto)" +
                " values (?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try {
            // prepared statement para inserção
            PreparedStatement stmt = connection.prepareStatement(sql);

            // seta os valores
            stmt.setString(1,animal.getApelido());
            stmt.setLong(2,animal.getIdDono());
            stmt.setString(3,animal.getSexo());
            stmt.setString(4,animal.getRaca());
            stmt.setString(5,animal.getTipoAnimal());
            stmt.setString(6,animal.getPorte());
            stmt.setInt(7,animal.getIdade());
            stmt.setInt(8,animal.getTempoProcurando());
            stmt.setString(9,animal.getVermifugado());
            stmt.setString(10,animal.getCastrado());
            stmt.setString(11,animal.getVacinado());
            stmt.setString(12,animal.getInformacoes());
            stmt.setString(13,animal.getFoto());
            
            // executa
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    public List<Animal> getLista() {
        try {
            List<Animal> animais = new ArrayList<Animal>();
            PreparedStatement stmt = this.connection.
                    prepareStatement("select * from animais inner join(donos) on (animais.fkdono = donos.iddono) order by tempoProcurando ASC");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                // criando o objeto dono
                Animal animal = new Animal();
                animal.setIdanimal(rs.getLong("idanimal"));
                animal.setApelido(rs.getString("apelido"));
                animal.setIdDono(rs.getLong("idDono"));
                animal.setSexo(rs.getString("sexo"));
                animal.setRaca(rs.getString("raca"));
                animal.setTipoAnimal(rs.getString("tipoAnimal"));
                animal.setPorte(rs.getString("porte"));
                animal.setIdade(rs.getInt("idade"));
                animal.setTempoProcurando(rs.getInt("tempoProcurando"));
                animal.setVermifugado(rs.getString("vermifugado"));
                animal.setCastrado(rs.getString("castrado"));
                animal.setVacinado(rs.getString("vacinado"));
                animal.setInformacoes(rs.getString("informacoes"));
                animal.setFoto(rs.getString("foto"));

                // adicionando o objeto � lista
                animais.add(animal);
            }
            rs.close();
            stmt.close();
            return animais;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
