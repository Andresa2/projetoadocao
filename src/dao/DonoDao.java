package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.ConnectionFactory;
import modelo.Dono;

public class DonoDao {

	
	// a conex�o com o banco de dados
    private Connection connection;

    public DonoDao() {
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public void adiciona(Dono dono) {
        String sql = "insert into donos " +
                "(nome,email,senha,telefone,celular,rua,bairro,cidade,estado)" +
                " values (?,?,?,?,?,?,?,?,?)";

        try {
            // prepared statement para inser��o
            PreparedStatement stmt = connection.prepareStatement(sql);

            // seta os valores
            stmt.setString(1,dono.getNome());
            stmt.setString(2,dono.getEmail());
            stmt.setString(3,dono.getSenha());
            stmt.setString(4,dono.getTelefone());
            stmt.setString(5,dono.getCelular());
            stmt.setString(6,dono.getRua());
            stmt.setString(7,dono.getBairro());
            stmt.setString(8,dono.getCidade());
            stmt.setString(9,dono.getEstado());
            
            // executa
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    public List<Dono> getLista() {
        try {
            List<Dono> donos = new ArrayList<Dono>();
            PreparedStatement stmt = this.connection.
                    prepareStatement("select * from donos");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                // criando o objeto dono
                Dono dono = new Dono();
                dono.setIdDono(rs.getLong("idDono"));
                dono.setNome(rs.getString("nome"));
                dono.setEmail(rs.getString("email"));
                dono.setSenha(rs.getString("senha"));
                dono.setTelefone(rs.getString("telefone"));
                dono.setCelular(rs.getString("celular"));
                dono.setRua(rs.getString("rua"));
                dono.setBairro(rs.getString("bairro"));
                dono.setCidade(rs.getString("cidade"));
                dono.setEstado(rs.getString("estado"));

                // adicionando o objeto � lista
                donos.add(dono);
            }
            rs.close();
            stmt.close();
            return donos;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
}
