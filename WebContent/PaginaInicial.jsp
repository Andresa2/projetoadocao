<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="dao" class="dao.DonoDao" />
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>AdoC�o</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<body>
	<header class="header-container" title="P�gina Inicial">
		<div class="topo-principal">
			<img id="logoPrincipal" src="media/animais.png" alt="Logo AdoCao">
			<img id="logoSecundario" src="media/logo.png" alt="Logo AdoCao">

			<div>
				<!--Menu de Navega��o-->
				<nav class="navbar__menu">
					<a href="QuemSomos.html" class="navbar" title="Quem somos">Quem
						somos</a> <a href="AdicionaDono.html" class="navbar" title="Cadastro">Fa�a
						seu cadastro para doar</a> <a href="AdotarAnimalMaisVelho.html"
						class="navbar" title="adotarAnimalMaisVelho">Por que adotar
						animais mais velhos?</a>
				</nav>
			</div>
		</div>
	</header>

	<section class="header-container">
		<div class="conteudo-principal">


			<div class="tabela">

				<c:forEach var="dono" items="${dao.lista}">
					<div class='lista'>
						${dono.nome} ${dono.email}
						<c:forEach items="${c.animais}" var="d">
							${d.apelido}
							${d.idade}
						</c:forEach>
					</div>
				</c:forEach>
			



			<%-- <%
					DonoDao dao = new DonoDao();
					List<Dono> donos = dao.getLista();

					for (Dono dono : donos) {
				%>
				<div class="lista">

					<p>
						<b>Nome: </b><%=dono.getNome()%></p>


					<%
						AnimalDao daoan = new AnimalDao();
						List<Animal> animais = daoan.getLista();
	
						for (Animal animal : animais) {
					%>


					<img src="<%=animal.getFoto()%>">
					<p><b>Nome: </b><%=animal.getApelido()%></p>
					<p><b>Sexo: </b><%=animal.getSexo()%></p>
					<p><b>Ra�a: </b><%=animal.getRaca()%></p>
					<p><b>Tipo de Animal: </b><%=animal.getTipoAnimal()%></p>
					<p><b>Porte: </b><%=animal.getPorte()%></p>
					<p><b>Idade: </b><%=animal.getIdade()%></p>
					<p><b>Meses procurando um dono: </b><%=animal.getTempoProcurando()%></p>
					<p><b>Vermifugado: </b><%=animal.getVermifugado()%></p>
					<p><b>Castrado: </b><%=animal.getCastrado()%></p>
					<p><b>Vacinado: </b><%=animal.getVacinado()%></p>
					<p><b>Mais informa��es: </b><%=animal.getInformacoes()%></p>
					<%
						}
					%>
				</div>
				<%
					} 
				%>--%>
		</div>
		</div>
	</section>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(event) {

	});
</script>
<style>
.lista {
	float: right;
	margin-bottom: 70px;
	border: 3px solid #6ec7cb;
	padding: 50px;
	border-radius: 132px;
	box-shadow: 5px 10px 8px #6ec7cb;
}

.lista>img {
	border-radius: 70px;
	border: 2px black solid;
	width: 250px;
	height: 150px;
}

.tabela {
	padding: 0px 70px 0px 70px;
	display: table;
}

p, img {
	display: initial;
	font-size: 16px;
	margin-right: 25px;
	float: left;
}

h1 {
	margin-top: 60px;
	font-family: cursive;
	text-align: center;
}

.navbar {
	margin-left: 25px;
	text-decoration: none;
	color: #6ec7cb;
	font-size: 20px;
}

nav.navbar__menu>a:hover {
	color: blue;
}

.navbar__menu {
	clear: right;
	float: right;
	margin-right: 40px;
	margin-top: 10px;
}

#logoPrincipal {
	width: 410px;
	margin-left: 15px;
	margin-top: 5px;
	border-radius: 40px;
	float: left;
}

#logoSecundario {
	margin-right: 15px;
	margin-top: 25px;
	float: right;
	width: 296px;
	height: 97px;
}

.header-container::after {
	align-content: center;
	width: 100%;
	margin-left: 20px;
	margin-right: 20px;
	height: 170px;
}

.topo-principal, .conteudo-principal {
	max-width: 1200px;
	padding-bottom: 30px;
	margin-left: auto;
	margin-right: auto;
	background-color: white;
	border-radius: 40px;
}

.topo-principal {
	height: 140px;
}

.conteudo-principal {
	padding-top: 60px;
	margin-top: 40px;
}

body {
	background-repeat: repeat;
	background: url("media/fundo.jpg");
}
</style>